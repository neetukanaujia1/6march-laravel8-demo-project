<?php

namespace App\Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{

    protected $table = 'brand';
}
