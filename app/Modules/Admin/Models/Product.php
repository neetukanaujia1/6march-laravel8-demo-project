<?php

namespace App\Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = 'products';
    protected $with = ['category','brand'];
    public function category(){
        return $this->hasOne(Category::class,'id','category_id');
    }

    public function brand(){
        return $this->hasOne(Brand::class,'id','brand_id');
    }

}
