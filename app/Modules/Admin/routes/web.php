<?php
use Illuminate\Support\Facades\Route;
Route::group(['prefix' => 'admin'], function () {
    #Login Routes
    Route::get('/login', 'LoginController@login');
    Route::post('/login', 'LoginController@login');
    Route::post('/logout', 'LoginController@logout');
    Route::group(['middleware' => 'admincheck'], function () {
        #Products Routes
        Route::get('/get-products', 'ProductController@getProducts');
        Route::post('/get-products', 'ProductController@getProducts');
        Route::post('/delete-product', 'ProductController@deleteProducts');
        Route::post('/edit-single-brand', 'ProductController@editSingleBrand');
        Route::post('/edit-single-category', 'ProductController@editSingleCategory');
        Route::post('/edit-product', 'ProductController@editProduct');
        Route::post('/create-product', 'ProductController@createProduct');
        #PlacedOrdes Routes
        Route::get('/get-placed-orders', 'ProductController@getPlacedOrders');
        Route::post('/get-placed-orders', 'ProductController@getPlacedOrders');
    });
});
