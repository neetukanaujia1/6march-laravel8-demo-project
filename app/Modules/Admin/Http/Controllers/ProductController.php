<?php
namespace App\Modules\Admin\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Brand;
use App\Modules\Admin\Models\Category;
use App\Modules\Admin\Models\Product;
use App\Modules\User\Models\PlacedOrders;
use Illuminate\Http\Request;
class ProductController extends Controller
{
    public function getProducts(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                return response()->json([
                    'status' => true,
                    'data' => Product::all()->toArray(),
                    'category' => Category::all()->toArray(),
                    'brand' => Brand::all()->toArray(),
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => false,
                    'msg' => $e->getMessage(),
                ]);
            }
        } else {
            return view('Admin::productDetails');
        }
    }
    public function getPlacedOrders(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                return response()->json([
                    'status' => true,
                    'data' => PlacedOrders::all()->toArray(),
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => false,
                    'msg' => $e->getMessage(),
                ]);
            }
        } else {
            return view('Admin::placedOrder');
        }
    }
    public function editProduct(Request $request)
    {
//        $request->validate([
//            'image' => ['required', 'image', 'mimes:jpg,png,jpeg,gif', 'max:4096'],
//            'category_id'=> 'required',
//            'brand_id'=> 'required',
//            'img'=> 'required',
//            'name'=> 'required',
//            'price'=> 'required',
//        ]);
        $file = $request->file('image');
        $image_name = $file->getClientOriginalName();
        $filePath = '/product-image/' . $image_name;
        $request->image->move(public_path("/product-image"), $image_name);
        Product::where('id', (int)$request->id)->update([
            'category_id' => $request->category_id,
            'brand_id' => $request->brand_id,
            'img' => $filePath,
            'name' => $request->name,
            'price' => $request->price,
        ]);
        return response()->json([
            'status' => true,
            'filePath' => $filePath
        ]);
    }
    public function createProduct(Request $request)
    {
//        $request->all();
//        $request->validate([
//            'image' => 'required|image|mimes:jpg,png,jpeg,gif|max:4096',
//            'category_id'=> 'required',
//            'brand_id'=> 'required',
//            'img'=> 'required',
//            'name'=> 'required',
//            'price'=> 'required',
//            ]);
        $file = $request->file('image');
        $image_name = $file->getClientOriginalName();
        $filePath = '/product-image/' . $image_name;
        $request->image->move(public_path("/product-image"), $image_name);
        Product::insert([
            'category_id' => $request->category_id,
            'brand_id' => $request->brand_id,
            'img' => $filePath,
            'name' => $request->name,
            'price' => $request->price,
        ]);
        return response()->json([
            'status' => true,
            'filePath' => $filePath
        ]);
    }
    public function deleteProducts(Request $request)
    {
        return response()->json([
            'status' => Product::destroy($request->id),
        ]);
    }
    public function editSingleBrand(Request $request)
    {
        return response()->json([
            'status' => Brand::where('id', $request->id)->update(['name' => $request->brand_name]),
        ]);
    }
    public function editSingleCategory(Request $request)
    {
        return response()->json([
            'status' => Category::where('id', $request->id)->update(['name' => $request->category_name]),
        ]);
    }
    public function test()
    {
        $q = Product::all()->toArray();
        dd('here', $q);
    }
}
