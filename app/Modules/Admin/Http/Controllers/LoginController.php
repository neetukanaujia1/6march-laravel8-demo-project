<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
class LoginController extends Controller
{
    public function login(Request $request)
    {

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required',
            ], [
                'email.required' => 'please enter your email',
                'password.required' => 'please enter your password',
            ]);
            if ($validator->fails()) {
                return response([
                    'errors' => $validator->errors(),
                    'status' => false,
                    'message' => 'Validation Error',
                    'code' => 201
                ]);
            }
            $credentials = request(['email', 'password']);
            if (!auth()->attempt($credentials)) {
                return response()->json([
                    'message' => 'Unauthorized',
                    'status'=>false,
                ], 401);
            }
            $user = $request->user();
            $request->session()->put('admin_data', $user);
            return response()->json([
                'user' => $user,
                'status'=>true,
            ]);
        } else {
            return view('Admin::login');
        }
    }
    public function logout(Request $request)
    {
        Session::remove('admin_data');
        return response()->json([
            'status' => !Session::has('admin_data'),
        ]);
    }
}

