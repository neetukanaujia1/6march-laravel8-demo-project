toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "200",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
function successToster(msg) {
    toastr.success(msg);
}
function errorToster(msg) {
    toastr.error(msg)
}
function infoToster(msg) {
    toastr.info(msg);
}
function warningToster(msg) {
    toastr.warning(msg);
}
