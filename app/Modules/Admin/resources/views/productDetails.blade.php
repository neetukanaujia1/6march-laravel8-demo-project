<html>
<head>
    <meta charset="utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Products</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://www.dukelearntoprogram.com/course1/common/js/image/SimpleImage.js">
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>
<style>
    /* BASIC */
    html {
        background-color: #56baed;
    }
    body {
        font-family: "Poppins", sans-serif;
        height: 100vh;
    }
    a {
        color: #92badd;
        display: inline-block;
        text-decoration: none;
        font-weight: 400;
    }
    h2 {
        text-align: center;
        font-size: 16px;
        font-weight: 600;
        text-transform: uppercase;
        display: inline-block;
        margin: 40px 8px 10px 8px;
        color: #cccccc;
    }
    /* STRUCTURE */
    .wrapper {
        display: flex;
        align-items: center;
        flex-direction: column;
        justify-content: center;
        width: 100%;
        min-height: 100%;
        padding: 20px;
    }
    #formContent {
        -webkit-border-radius: 10px 10px 10px 10px;
        border-radius: 10px 10px 10px 10px;
        background: #fff;
        padding: 30px;
        width: 100%;
        height: 100%;
        max-width: 90%;
        position: relative;
        padding: 0px;
        -webkit-box-shadow: 0 30px 60px 0 rgba(0, 0, 0, 0.3);
        box-shadow: 0 30px 60px 0 rgba(0, 0, 0, 0.3);
        text-align: center;
        height: 100vh;
    }
    #formFooter {
        background-color: #f6f6f6;
        border-top: 1px solid #dce8f1;
        padding: 25px;
        text-align: center;
        -webkit-border-radius: 0 0 10px 10px;
        border-radius: 0 0 10px 10px;
    }
    /* TABS */
    h2.inactive {
        color: #cccccc;
    }
    h2.active {
        color: #0d0d0d;
        border-bottom: 2px solid #5fbae9;
    }
    /* FORM TYPOGRAPHY*/
    input[type=button], input[type=submit], input[type=reset] {
        background-color: #56baed;
        border: none;
        color: white;
        padding: 15px 80px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        text-transform: uppercase;
        font-size: 13px;
        -webkit-box-shadow: 0 10px 30px 0 rgba(95, 186, 233, 0.4);
        box-shadow: 0 10px 30px 0 rgba(95, 186, 233, 0.4);
        -webkit-border-radius: 5px 5px 5px 5px;
        border-radius: 5px 5px 5px 5px;
        margin: 5px 20px 40px 20px;
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }
    input[type=button]:hover, input[type=submit]:hover, input[type=reset]:hover {
        background-color: #39ace7;
    }
    input[type=button]:active, input[type=submit]:active, input[type=reset]:active {
        -moz-transform: scale(0.95);
        -webkit-transform: scale(0.95);
        -o-transform: scale(0.95);
        -ms-transform: scale(0.95);
        transform: scale(0.95);
    }
    input[type=text] {
        background-color: #f6f6f6;
        border: none;
        color: #0d0d0d;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 5px;
        width: 85%;
        border: 2px solid #f6f6f6;
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
        -webkit-border-radius: 5px 5px 5px 5px;
        border-radius: 5px 5px 5px 5px;
    }
    select {
        background-color: #f6f6f6;
        border: none;
        color: #0d0d0d;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 5px;
        width: 85%;
        border: 2px solid #f6f6f6;
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
        -webkit-border-radius: 5px 5px 5px 5px;
        border-radius: 5px 5px 5px 5px;
    }
    input[type=text]:focus {
        background-color: #fff;
        border-bottom: 2px solid #5fbae9;
    }
    input[type=text]:placeholder {
        color: #cccccc;
    }
    /* ANIMATIONS */
    /* Simple CSS3 Fade-in-down Animation */
    .fadeInDown {
        -webkit-animation-name: fadeInDown;
        animation-name: fadeInDown;
        -webkit-animation-duration: 1s;
        animation-duration: 1s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
    }
    @-webkit-keyframes fadeInDown {
        0% {
            opacity: 0;
            -webkit-transform: translate3d(0, -100%, 0);
            transform: translate3d(0, -100%, 0);
        }
        100% {
            opacity: 1;
            -webkit-transform: none;
            transform: none;
        }
    }
    @keyframes fadeInDown {
        0% {
            opacity: 0;
            -webkit-transform: translate3d(0, -100%, 0);
            transform: translate3d(0, -100%, 0);
        }
        100% {
            opacity: 1;
            -webkit-transform: none;
            transform: none;
        }
    }
    /* Simple CSS3 Fade-in Animation */
    @-webkit-keyframes fadeIn {
        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }
    @-moz-keyframes fadeIn {
        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }
    @keyframes fadeIn {
        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }
    .fadeIn {
        opacity: 0;
        -webkit-animation: fadeIn ease-in 1;
        -moz-animation: fadeIn ease-in 1;
        animation: fadeIn ease-in 1;
        -webkit-animation-fill-mode: forwards;
        -moz-animation-fill-mode: forwards;
        animation-fill-mode: forwards;
        -webkit-animation-duration: 1s;
        -moz-animation-duration: 1s;
        animation-duration: 1s;
    }
    .fadeIn.first {
        -webkit-animation-delay: 0.4s;
        -moz-animation-delay: 0.4s;
        animation-delay: 0.4s;
    }
    .fadeIn.second {
        -webkit-animation-delay: 0.6s;
        -moz-animation-delay: 0.6s;
        animation-delay: 0.6s;
    }
    .fadeIn.third {
        -webkit-animation-delay: 0.8s;
        -moz-animation-delay: 0.8s;
        animation-delay: 0.8s;
    }
    .fadeIn.fourth {
        -webkit-animation-delay: 1s;
        -moz-animation-delay: 1s;
        animation-delay: 1s;
    }
    /* Simple CSS3 Fade-in Animation */
    .underlineHover:after {
        display: block;
        left: 0;
        bottom: -10px;
        width: 0;
        height: 2px;
        background-color: #56baed;
        content: "";
        transition: width 0.2s;
    }
    .underlineHover:hover {
        color: #0d0d0d;
    }
    .underlineHover:hover:after {
        width: 100%;
    }
    /* OTHERS */
    *:focus {
        outline: none;
    }
    #icon {
        width: 60%;
    }
    .header {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    }
    .sub-header {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        order: 1
    }
    .inputfields {
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        order: 1
    }
    .red-star {
        color: red;
    }
    canvas {
        height: 175px;
        border-width: 1px;
        border-color: black;
        background-color: #56baed;
        color: white;
        padding: 15px 80px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        text-transform: uppercase;
        font-size: 13px;
        -webkit-box-shadow: 0 10px 30px 0 rgba(95, 186, 233, 0.4);
        box-shadow: 0 10px 30px 0 rgba(95, 186, 233, 0.4);
        -webkit-border-radius: 5px 5px 5px 5px;
        border-radius: 5px 5px 5px 5px;
        margin: 5px 20px 40px 20px;
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }
</style>
<div class="wrapper fadeInDown">
    <div id="formContent">
        <div class="fadeIn first">
            <div class="header">
                <h4 style="padding: 25px">Product details</h4>
                <div class="sub-header">
                    <button style="margin-top: 5px; margin-bottom: 5px;margin-right: 1px;"  data-toggle="modal" data-target="#createProductModal" class="btn btn-info">Create
                        Product
                    </button>
                    <button style="margin-top: 5px; margin-bottom: 5px;margin-right: 1px;" class="btn btn-info" onclick="editBrandModal()" data-toggle="modal" data-target="#editBrandModel">Edit Brand
                    </button>
                    <button style="margin-top: 5px; margin-bottom: 5px;margin-right: 1px;" class="btn btn-info" onclick="editCategoryModal()" data-toggle="modal" data-target="#editCategoryModel">Edit Category</button>
                    <button style="margin-top: 5px; margin-bottom: 5px;margin-right: 1px;" class="btn btn-info"
                            onclick="redirectToViewOrder()">View Orders
                    </button>
                    <button style="margin-top: 5px; margin-bottom: 5px;margin-right: 1px;" class="btn btn-info"
                            onclick="handelLogout()">Logout
                    </button>
                </div>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Image</th>
                <th scope="col">Name</th>
                <th scope="col">Category</th>
                <th scope="col">Brand</th>
                <th scope="col">Cost (Rs)</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody id="product-data">
            </tbody>
        </table>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="editProductModal" tabindex="-1" role="dialog" aria-labelledby="editProductModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editProductModalCenterTitle">Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="product-form">
                <canvas id="canv1"></canvas>
                <p>
                    Select Product Image:
                    <input type="file" multiple="false" accept="image/*" id=finput onchange="upload()">
                </p>
                <div class="inputfields">
                    <div class="error-div-name" style="display: none"><span class="red-star">★</span><span
                            id='error_name' class="span-error" style="color: red;">required</span></div>
                    <input type="text" id="name" name="name" placeholder="Name">
                </div>
                <div class="inputfields">
                    <div class="error-div-price" style="display: none"><span class="red-star">★</span><span
                            id='error_price' class="span-error" style="color: red;">required</span></div>
                    <input type="text" id="price" name="price" placeholder="Price">
                </div>
                <div class="inputfields">
                    <div class="error-div-brand_id" style="display: none"><span class="red-star">★</span><span
                            id='error_brand_id' class="span-error" style="color: red;">required</span></div>
                    <select name="brand_id" class="brand_id"></select>
                </div>
                <div class="inputfields">
                    <div class="error-div-category_id" style="display: none"><span class="red-star">★</span><span
                            id='error_category_id' class="span-error" style="color: red;">required</span></div>
                    <select name="category_id" class="category_id"></select>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="edit-product-modal-close-btn" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id='product-edit-btn' class="btn btn-primary">Edit
                </button>
            </div>
        </div>
    </div>
</div>







<!-- createvModal -->
<div class="modal fade" id="createProductModal" tabindex="-1" role="dialog" aria-labelledby="createProductModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createProductModalCenterTitle">Create</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="product-form">
                    <canvas id="canv2"></canvas>
                    <p>
                        Select Product Image:
                        <input type="file" multiple="false" accept="image/*" id=finput_new onchange="upload_new()">
                    </p>
                    <div class="inputfields">
                        <div class="error-div-name" style="display: none"><span class="red-star">★</span><span
                                id='error_name' class="span-error" style="color: red;">required</span></div>
                        <input type="text" id="name_new" name="name" placeholder="Name">
                    </div>
                    <div class="inputfields">
                        <div class="error-div-price" style="display: none"><span class="red-star">★</span><span
                                id='error_price' class="span-error" style="color: red;">required</span></div>
                        <input type="text" id="price_new" name="price" placeholder="Price">
                    </div>
                    <div class="inputfields">
                        <div class="error-div-brand_id" style="display: none"><span class="red-star">★</span><span
                                id='error_brand_id' class="span-error" style="color: red;">required</span></div>
                        <select class="brand_id"  id="brand_id_new"></select>
                    </div>
                    <div class="inputfields">
                        <div class="error-div-category_id" style="display: none"><span class="red-star">★</span><span
                                id='error_category_id' class="span-error" style="color: red;">required</span></div>
                        <select class="category_id" name="category_id_new" id="category_id_new"></select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="create-product-modal-close-btn" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id='product-create-btn' class="btn btn-primary">Create
                </button>
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="editBrandModel" tabindex="-1" role="dialog" aria-labelledby="addBrandModelLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addBrandModelLabel">Brand</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="brand_table">
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" id="edit-brand-modal-close-btn" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editCategoryModel" tabindex="-1" role="dialog" aria-labelledby="editCategoryModellabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editCategoryModellabel">Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="category_table">
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" id="edit-category-modal-close-btn" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{{--------------------------------------------------------------------------------------------------------------------------}}
<script>
    let __brand = [];
    let __category = [];
    let __edit_product_id = null;
    $(document).ready(function () {
        getProducts();
    });
    function getProducts(){
        $.ajax({
            url: '/admin/get-products',
            dataType: 'json',
            type: 'post',
            data: {
                _token: '{{csrf_token()}}',
            },
            success: function (response) {
                if (response.status) {
                    let temp = '';
                    let temp_brand = ' <option value="" selected disabled hidden>Choose here</option>';
                    let temp_category = ' <option value="" selected disabled hidden>Choose here</option>';
                    __brand = response.brand;
                    __category = response.category;
                    __brand.map(data => {
                        temp_brand += `<option value=` + data.id + `>` + data.name + `</option>`
                    })
                    $('.brand_id').html(temp_brand);
                    __category.map(data => {
                        temp_category += `<option value=` + data.id + `>` + data.name + `</option>`
                    })
                    $('.category_id').html(temp_category);
                    response.data.map((data, index) => {
                        temp += `         <tr>
                <th scope=` + index + `>` + (parseInt(index) + 1) + `</th>
                <td> <img style="height: 100;
width: 100;" src=" ` + data.img + `"   alt="no img"  /></td>
                <td>` + data.name + `</td>
                <td>` + data.category.name + `</td>
                <td>` + data.brand.name + `</td>
                <td>` + data.price + `</td>
       <td>
<button onclick="editProduct(` + data.id + `)"type="button" class="btn btn-primary btn" data-toggle="modal" data-target="#editProductModal">
  <i class="fa fa-edit"></i>
</button>
<button class="btn" onclick="deleteProduct(` + data.id + `)"><i class="fa fa-trash"></i></button></td>
            </tr>`
                    });
                    $('#product-data').html(temp);
                } else {
                }
            }
        })
    }
    $('#product-edit-btn').click(function (e) {
        e.preventDefault();
        var formData = new FormData();
        formData.append('id', __edit_product_id);
        formData.append('name', $('#name').val());
        formData.append('price', $('#price').val());
        formData.append('brand_id', $('#brand_id').val());
        formData.append('category_id', $('#category_id').val());
        formData.append('image', $("#finput")[0].files[0]);
        $.ajax({
            url: '/admin/edit-product',
            dataType: 'json',
            type: 'post',
            processData : false,
            cache: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            data: formData,
            success: function (response){
                if(response.status){
                    getProducts();
                    $('#edit-product-modal-close-btn').click();
                    Swal.fire('Edit Suceesfully!', '', 'success')
                }
            }
        });
    });




    $('#product-create-btn').click(function (e) {
        e.preventDefault();
        var formData = new FormData();
        formData.append('name', $('#name_new').val());
        formData.append('price', $('#price_new').val());
        formData.append('brand_id', $('#brand_id_new').val());
        formData.append('category_id', $('#category_id_new').val());
        formData.append('image', $("#finput_new")[0].files[0]);
        $.ajax({
            url: '/admin/create-product',
            dataType: 'json',
            type: 'post',
            processData : false,
            cache: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            data: formData,
            success: function (response){
                if(response.status){
                    getProducts();
                    $('#create-product-modal-close-btn').click();
                    Swal.fire('Edit Suceesfully!', '', 'success')
                }
            }
        });
    });
    function editProduct(id) {
        __edit_product_id=id;
    }
    function deleteProduct(id) {
        Swal.fire({
            title: 'Do you want to delete the product ?',
            showDenyButton: true,
            confirmButtonText: 'Delete',
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    url: '/admin/delete-product',
                    dataType: 'json',
                    type: 'post',
                    data: {
                        id :id,
                        _token: '{{csrf_token()}}',
                    },
                    success: function (response) {
                        if(response.status){
                            Swal.fire('Deleted!', '', 'success')
                            getProducts();
                        }
                    }
                });
            }
        })
    }
    function redirectToViewOrder() {
        window.location = "/admin/get-placed-orders"
    }
    function upload() {
        var imgcanvas = document.getElementById("canv1");
        var fileinput = document.getElementById("finput");
        var image = new SimpleImage(fileinput);
        image.drawTo(imgcanvas);
    }
    function upload_new() {
        var imgcanvas = document.getElementById("canv2");
        var fileinput = document.getElementById("finput_new");
        var image = new SimpleImage(fileinput);
        image.drawTo(imgcanvas);
    }
    function editBrandModal() {
let temp='';
        __brand.map(data=>{
   temp += `<tr>
                        <td><input type='text' value="`+data.name+  `" /></td>
                        <td><button  product-id="`+data.id+  `" class="btn btn-primary btn editSingleBrand">  <i class="fa fa-edit"></i></button></td>
                    </tr>`
});
        $('#brand_table').html(temp);
    }
    function editCategoryModal() {
let temp='';
        __category.map(data=>{
   temp += `<tr>
                        <td><input type='text' value="`+data.name+  `" /></td>
                        <td><button  product-id="`+data.id+  `" class="btn btn-primary btn editSingleCategory">  <i class="fa fa-edit "></i></button></td>
                    </tr>`
});
        $('#category_table').html(temp);
    }
    $(document.body).on('click', '.editSingleBrand', function (e) {
        var self = $(this);
        $.ajax({
            url: '/admin/edit-single-brand',
            dataType: 'json',
            type: 'post',
            data: {
                id:self.attr('product-id'),
                brand_name: self.parent().parent().find('input').val(),
                _token: '{{csrf_token()}}',
            },
            success: function (response) {
                if(response.status){
                    getProducts();
                    $('#edit-brand-modal-close-btn').click();
                    Swal.fire('Edit Successfully!', '', 'success')

                }
            }
        });


    });




    $(document.body).on('click', '.editSingleCategory', function (e) {
        var self = $(this);
        $.ajax({
            url: '/admin/edit-single-category',
            dataType: 'json',
            type: 'post',
            data: {
                id:self.attr('product-id'),
                category_name: self.parent().parent().find('input').val(),
                _token: '{{csrf_token()}}',
            },
            success: function (response) {
                if(response.status){
                    getProducts();
                    $('#edit-category-modal-close-btn').click();
                    Swal.fire('Edit Successfully!', '', 'success')

                }
            }
        });


    });

    function editSingleCategory(id) {
        $.ajax({
            url: '/admin/edit-single-category',
            dataType: 'json',
            type: 'post',
            data: {
                id:id,
                _token: '{{csrf_token()}}',
            },
            success: function (response) {
            }
        });
    }

    function handelLogout() {
        $.ajax({
            url: '/admin/logout',
            type: 'post',
            data: {
                _token: '{{csrf_token()}}',
            },
            success: function (response) {
                if(response.status){
                    window.location = "/admin/login"
                }
            }
        });
    }
</script>
</html>
