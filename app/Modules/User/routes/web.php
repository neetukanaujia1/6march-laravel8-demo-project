<?php
use Illuminate\Support\Facades\Route;
#Login Routes
Route::get('/', 'LoginController@login');
Route::post('/login', 'LoginController@login');
#registration Routes
Route::get('/registration', 'LoginController@registration');
Route::post('/registration', 'LoginController@registration');
Route::post('/logout', 'LoginController@logout');
Route::group(['middleware' => 'usercheck'], function () {
    #Products Routes
    Route::get('/dashboard', 'OrderController@getDashboard');
    Route::post('/dashboard', 'OrderController@getDashboard');
    Route::post('/placed-order', 'OrderController@placedOrder');
    Route::post('/get-products', 'OrderController@getProducts');
});
