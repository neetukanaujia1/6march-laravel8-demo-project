<?php
namespace App\Modules\User\Models;
use App\Models\User;
use App\Modules\Admin\Models\Product;
use Illuminate\Database\Eloquent\Model;
class PlacedOrders extends Model
{
    protected $table = 'placed_orders';
    protected $with = ['product'];
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
