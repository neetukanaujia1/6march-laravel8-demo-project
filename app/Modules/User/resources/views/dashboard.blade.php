<html>
<head>
    <meta charset="utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Dashboard</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>
<style>
    /* BASIC */
    html {
        background-color: #56baed;
    }
    body {
        font-family: "Poppins", sans-serif;
        height: 100vh;
    }
    a {
        color: #92badd;
        display: inline-block;
        text-decoration: none;
        font-weight: 400;
    }
    h2 {
        text-align: center;
        font-size: 16px;
        font-weight: 600;
        text-transform: uppercase;
        display: inline-block;
        margin: 40px 8px 10px 8px;
        color: #cccccc;
    }
    /* STRUCTURE */
    .wrapper {
        display: flex;
        align-items: center;
        flex-direction: column;
        justify-content: center;
        width: 100%;
        min-height: 100%;
        padding: 20px;
    }
    #formContent {
        -webkit-border-radius: 10px 10px 10px 10px;
        border-radius: 10px 10px 10px 10px;
        background: #fff;
        padding: 30px;
        width: 100%;
        height: 100%;
        max-width: 90%;
        position: relative;
        padding: 0px;
        -webkit-box-shadow: 0 30px 60px 0 rgba(0, 0, 0, 0.3);
        box-shadow: 0 30px 60px 0 rgba(0, 0, 0, 0.3);
        text-align: center;
        height: 100vh;
    }
    #formFooter {
        background-color: #f6f6f6;
        border-top: 1px solid #dce8f1;
        padding: 25px;
        text-align: center;
        -webkit-border-radius: 0 0 10px 10px;
        border-radius: 0 0 10px 10px;
    }
    /* TABS */
    h2.inactive {
        color: #cccccc;
    }
    h2.active {
        color: #0d0d0d;
        border-bottom: 2px solid #5fbae9;
    }
    /* FORM TYPOGRAPHY*/
    input[type=button], input[type=submit], input[type=reset] {
        background-color: #56baed;
        border: none;
        color: white;
        padding: 15px 80px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        text-transform: uppercase;
        font-size: 13px;
        -webkit-box-shadow: 0 10px 30px 0 rgba(95, 186, 233, 0.4);
        box-shadow: 0 10px 30px 0 rgba(95, 186, 233, 0.4);
        -webkit-border-radius: 5px 5px 5px 5px;
        border-radius: 5px 5px 5px 5px;
        margin: 5px 20px 40px 20px;
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }
    input[type=button]:hover, input[type=submit]:hover, input[type=reset]:hover {
        background-color: #39ace7;
    }
    input[type=button]:active, input[type=submit]:active, input[type=reset]:active {
        -moz-transform: scale(0.95);
        -webkit-transform: scale(0.95);
        -o-transform: scale(0.95);
        -ms-transform: scale(0.95);
        transform: scale(0.95);
    }
    input[type=text] {
        background-color: #f6f6f6;
        border: none;
        color: #0d0d0d;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 5px;
        width: 85%;
        border: 2px solid #f6f6f6;
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
        -webkit-border-radius: 5px 5px 5px 5px;
        border-radius: 5px 5px 5px 5px;
        height: 5;
    }
    input[type=text]:focus {
        background-color: #fff;
        border-bottom: 2px solid #5fbae9;
    }
    input[type=text]:placeholder {
        color: #cccccc;
    }
    input[type=number] {
        height: 5;
        background-color: #f6f6f6;
        border: none;
        color: #0d0d0d;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 5px;
        width: 85%;
        border: 2px solid #f6f6f6;
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
        -webkit-border-radius: 5px 5px 5px 5px;
        border-radius: 5px 5px 5px 5px;
    }
    input[type=number]:focus {
        background-color: #fff;
        border-bottom: 2px solid #5fbae9;
    }
    input[type=number]:placeholder {
        color: #cccccc;
    }
    /* ANIMATIONS */
    /* Simple CSS3 Fade-in-down Animation */
    .fadeInDown {
        -webkit-animation-name: fadeInDown;
        animation-name: fadeInDown;
        -webkit-animation-duration: 1s;
        animation-duration: 1s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
    }
    @-webkit-keyframes fadeInDown {
        0% {
            opacity: 0;
            -webkit-transform: translate3d(0, -100%, 0);
            transform: translate3d(0, -100%, 0);
        }
        100% {
            opacity: 1;
            -webkit-transform: none;
            transform: none;
        }
    }
    @keyframes fadeInDown {
        0% {
            opacity: 0;
            -webkit-transform: translate3d(0, -100%, 0);
            transform: translate3d(0, -100%, 0);
        }
        100% {
            opacity: 1;
            -webkit-transform: none;
            transform: none;
        }
    }
    /* Simple CSS3 Fade-in Animation */
    @-webkit-keyframes fadeIn {
        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }
    @-moz-keyframes fadeIn {
        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }
    @keyframes fadeIn {
        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }
    .fadeIn {
        opacity: 0;
        -webkit-animation: fadeIn ease-in 1;
        -moz-animation: fadeIn ease-in 1;
        animation: fadeIn ease-in 1;
        -webkit-animation-fill-mode: forwards;
        -moz-animation-fill-mode: forwards;
        animation-fill-mode: forwards;
        -webkit-animation-duration: 1s;
        -moz-animation-duration: 1s;
        animation-duration: 1s;
    }
    .fadeIn.first {
        -webkit-animation-delay: 0.4s;
        -moz-animation-delay: 0.4s;
        animation-delay: 0.4s;
    }
    .fadeIn.second {
        -webkit-animation-delay: 0.6s;
        -moz-animation-delay: 0.6s;
        animation-delay: 0.6s;
    }
    .fadeIn.third {
        -webkit-animation-delay: 0.8s;
        -moz-animation-delay: 0.8s;
        animation-delay: 0.8s;
    }
    .fadeIn.fourth {
        -webkit-animation-delay: 1s;
        -moz-animation-delay: 1s;
        animation-delay: 1s;
    }
    /* Simple CSS3 Fade-in Animation */
    .underlineHover:after {
        display: block;
        left: 0;
        bottom: -10px;
        width: 0;
        height: 2px;
        background-color: #56baed;
        content: "";
        transition: width 0.2s;
    }
    .underlineHover:hover {
        color: #0d0d0d;
    }
    .underlineHover:hover:after {
        width: 100%;
    }
    /* OTHERS */
    *:focus {
        outline: none;
    }
    #icon {
        width: 60%;
    }
    .header {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    }
    .inputfields {
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        order: 1
    }
    .red-star {
        color: red;
    }
</style>
<div class="wrapper fadeInDown">
    <div id="formContent">
        <div class="fadeIn first">
            <div class="header">
                <h4 style="padding: 25px">Products</h4>
                <button class="btn btn-info" onclick="handelLogout()" style="padding: 25px">Logout</button>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Image</th>
                <th scope="col">Name</th>
                <th scope="col">Category</th>
                <th scope="col">Brand</th>
                <th scope="col">Cost (Rs)</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody id="product-data">
            </tbody>
        </table>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="placedOrderModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Shipping Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="inputfields">
                    <div class="error-div-name" style="display: none"><span class="red-star">★</span><span
                            id='error_name' class="span-error" style="color: red;"></span></div>
                    <input type="text" id="name" class="fadeIn second" name="name" placeholder="Name">
                </div>
                <div class="inputfields">
                    <div class="error-div-email" style="display: none"><span class="red-star">★</span><span
                            id='error_email' class="span-error" style="color: red;">required</span></div>
                    <input type="text" id="email" class="fadeIn second" name="email" placeholder="Email">
                </div>
                <div class="inputfields">
                    <div class="error-div-phone" style="display: none"><span class="red-star">★</span><span
                            id='error_phone' class="span-error" style="color: red;">required</span></div>
                    <input type="text" id="phone" class="fadeIn third" name="phone" placeholder="Phone number">
                </div>
                <div class="inputfields">
                    <div class="error-div-address" style="display: none"><span class="red-star">★</span><span
                            id='error_address' class="span-error" style="color: red;">required</span></div>
                    <input type="text" id="address" class="fadeIn third" name="address" placeholder="Address">
                </div>
                <div class="inputfields">
                    <div class="error-div-city" style="display: none"><span class="red-star">★</span><span
                            id='error_city' class="span-error" style="color: red;">required</span></div>
                    <input type="text" id="city" class="fadeIn third" name="city" placeholder="City">
                </div>
                <div class="inputfields">
                    <div class="error-div-zipcode" style="display: none"><span class="red-star">★</span><span
                            id='error_zipcode' class="span-error" style="color: red;">required</span></div>
                    <input type="text" id="zipcode" class="fadeIn third" name="login" placeholder="Pin code">
                </div>
                <div class="inputfields">
                    <div class="error-div-quantity" style="display: none"><span class="red-star">★</span><span
                            id='error_quantity' class="span-error" style="color: red;">required</span></div>
                    <input type="number" id="quantity" min="1" max="5" class="fadeIn third" name="quantity"
                           placeholder="Quantity">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id='placedOrderModelCloseBtn' class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" onclick="handlePlacedOrder()" class="btn btn-primary">Placed Order</button>
            </div>
        </div>
    </div>
</div>
{{--------------------------------------------------------------------------------------------------------------------------}}
<script>
    let __id;
    $(document).ready(function () {
        $(':input').val('');
        $('.span-error').text('required');
    });
    function handlePlacedOrder() {
        $.ajax({
            url: '/placed-order',
            dataType: 'json',
            type: 'post',
            data: {
                name: $('#name').val(),
                phone: $('#phone').val(),
                email: $('#email').val(),
                address: $('#address').val(),
                city: $('#city').val(),
                zipcode: $('#zipcode').val(),
                quantity: $('#quantity').val(),
                product_id: __id,
                _token: '{{csrf_token()}}',
            },
            success: function (response) {
                if(response.status) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Order is placed successfully',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    $('#placedOrderModelCloseBtn').click();
                    $(':input').val('');
                    $('.span-error').text('required');
                }else {
                let errors = response.errors;
                if (('name' in errors)) {
                    $('#error_name').text(response.errors.name[0]);
                    $('.error-div-name').css('display', 'block');
                } else {
                    $('.error-div-name').css('display', 'none');
                }
                if (('email' in errors)) {
                    $('#error_email').text(response.errors.email[0])
                    $('.error-div-email').css('display', 'block');
                } else {
                    $('.error-div-email').css('display', 'none');
                }
                if (('phone' in errors)) {
                    $('#error_phone').text(response.errors.phone[0])
                    $('.error-div-phone').css('display', 'block');
                } else {
                    $('.error-div-phone').css('display', 'none');
                }
                if (('address' in errors)) {
                    $('#error_address').text(response.errors.address[0])
                    $('.error-div-address').css('display', 'block');
                } else {
                    $('.error-div-address').css('display', 'none');
                }
                if (('city' in errors)) {
                    $('#error_city').text(response.errors.city[0]);
                    $('.error-div-city').css('display', 'block');
                } else {
                    $('.error-div-city').css('display', 'none');
                }
                if (('zipcode' in errors)) {
                    $('#error_zipcode').text(response.errors.zipcode[0])
                    $('.error-div-zipcode').css('display', 'block');
                } else {
                    $('.error-div-zipcode').css('display', 'none');
                }
                if (('quantity' in errors)) {
                    $('#error_quantity').text(response.errors.quantity[0]);
                    $('.error-div-quantity').css('display', 'block');
                } else {
                    $('.error-div-quantity').css('display', 'none');
                }
                }
            }
        });
    }
    $(document).ready(function () {
        $.ajax({
            url: '/get-products',
            dataType: 'json',
            type: 'post',
            data: {
                _token: '{{csrf_token()}}',
            },
            success: function (response) {
                if (response.status) {
                    let temp = '';
                    response.data.map((data, index) => {
                        temp += `         <tr>
                <th scope=` + index + `>` + (parseInt(index) + 1) + `</th>
                <td> <img style="height: 100;
width: 100;" src=" ` + data.img + `"   alt="no img"  /></td>
                <td>` + data.name + `</td>
                <td>` + data.category.name + `</td>
                <td>` + data.brand.name + `</td>
                <td>` + data.price + `</td>
       <td>
<button type="button" onclick="buyProduct(` + data.id + `)" class="btn btn-primary" data-toggle="modal" data-target="#placedOrderModel">
 Buy
</button>
</td>
            </tr>`
                    });
                    $('#product-data').html(temp);
                } else {
                    errorToster(response['msg']);
                }
            }
        })
    });
    function buyProduct(id) {
 __id=id;
    }


    function handelLogout() {
        $.ajax({
            url: '/logout',
            type: 'post',
            data: {
                _token: '{{csrf_token()}}',
            },
            success: function (response) {
                if(response.status){
                    window.location = "/"
                }
            }
            });
    }




</script>
</html>
