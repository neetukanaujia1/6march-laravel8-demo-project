<?php
namespace App\Modules\User\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Brand;
use App\Modules\Admin\Models\Category;
use App\Modules\Admin\Models\Product;
use App\Modules\User\Models\PlacedOrders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class OrderController extends Controller
{
    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getDashboard(Request $request)
    {
        if ($request->isMethod('post')) {
            dd($request->all());
        } else {
            return view('User::dashboard');
        }
    }
    public function placedOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|min:3',
            'phone' => 'required|size:10',
            'address' => 'required',
            'email' => 'required|email',
            'city' => 'required',
            'zipcode' => 'required | size:6',
            'quantity' => 'required',
        ], [
            'name.required' => 'Please enter your name',
            'phone.required' => 'please enter your phone',
            'email.required' => 'please enter your email',
            'phone.size' => 'phone number should be 10 digit',
            'city.required' => 'please enter your city',
            'zipcode.required' => 'please enter your pincode',
            'zipcode.size' => 'pincode should be 6 digit',
            'quantity.required' => 'please enter your quantity',
        ]);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors(),
                'status' => false,
                'message' => 'Validation Error',
                'code' => 201
            ]);
        }
        return response([
            'status' => PlacedOrders::insert([
                'address' => $request->address,
                'city' => $request->city,
                'zipcode' => $request->zipcode,
                'name' => $request->name,
                'phone' => $request->phone,
                'product_id' => $request->product_id,
                'quantity' => $request->quantity,
            ]),
        ]);
    }
    public function getProducts(){
        try {
            return response()->json([
                'status' => true,
                'data' => Product::all()->toArray(),
                'category' => Category::all()->toArray(),
                'brand' => Brand::all()->toArray(),
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'msg' => $e->getMessage(),
            ]);
        }
    }
}
