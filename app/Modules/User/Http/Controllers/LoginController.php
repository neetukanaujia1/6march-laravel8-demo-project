<?php
namespace App\Modules\User\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
class LoginController extends Controller
{
    public function registration(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email|unique:users,email',
                'password' => 'required',
                'name' => 'required',
            ], [
                'name.required' => 'please enter your name',
                'email.required' => 'please enter your email',
                'password.required' => 'please enter your password',
            ]);
            if ($validator->fails()) {
                return response([
                    'errors' => $validator->errors(),
                    'status' => false,
                    'message' => 'Validation Error',
                    'code' => 201
                ]);
            }
            try {
                $user = new User();
                $user->name = $request->name;
                $user->email = $request->get('email');
                $user->password = Hash::make($request->get('password'));
                $user->ip_address = request()->ip();
                $user->verify_token = rand(100000, 999999);
                $user->save();
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->accessToken;
                $token->save();
                return response()->json([
                    'user' => $user,
                    'status' => true,
                ]);
            } catch (\Exception $exception) {
                return response()->json([
                    'error_code' => $exception->getCode(),
                    'error_message' => $exception->getMessage(),
                    'status' => false,
                ]);
            }
        } else {
            return view('User::registration');
        }
    }
    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required',
            ], [
                'email.required' => 'please enter your email',
                'password.required' => 'please enter your password',
            ]);
            if ($validator->fails()) {
                return response([
                    'errors' => $validator->errors(),
                    'status' => false,
                    'message' => 'Validation Error',
                    'code' => 201
                ]);
            }
            $credentials = request(['email', 'password']);
            if (!auth()->attempt($credentials)) {
                return response()->json([
                    'message' => 'Unauthorized',
                    'status' => false,
                ], 401);
            }
            $user = $request->user();
            $request->session()->put('user_data', $user);
            return response()->json([
                'user' => $user,
                'status' => true,
            ]);
        } else {
            return view('User::login');
        }
    }
    public function logout(Request $request)
    {
        Session::remove('user_data');
        return response()->json([
            'status' => !Session::has('user_data'),
        ]);
    }
}
