
# Getting started

## Installation

 repository

    git clone https://neetukanaujia1@bitbucket.org/neetukanaujia1/6march-laravel8-demo-project.git


Install Laravel the dependencies using composer

    composer install
   

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Run Seeders
    
    php artisan db:seed
    
Create "personal access" and "password grant" (Laravel Passport)
                
    php artisan passport:install
	
 Laravel project run for local server (http://127.0.0.1:8000)
                
    php artisan serve
    

    
