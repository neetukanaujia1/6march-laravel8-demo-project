<?php

namespace Database\Seeders;



use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Role::class);
        $this->call(Category::class);
        $this->call(Brand::class);
        $this->call(Products::class);
        $this->call(Admin::class);
    }
}
