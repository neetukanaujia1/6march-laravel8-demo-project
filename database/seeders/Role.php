<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Role extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'admin',
            'user',
        ];
        foreach ($data as $role) {
            \App\Modules\Admin\Models\Role::insert([
                'name' => $role,
            ]);
        }
    }
}
