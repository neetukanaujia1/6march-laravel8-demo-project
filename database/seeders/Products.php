<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
class Products extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            1=>[
                'name' => 'Watch Blaze',
                'category_id' => 1,
                'brand_id' =>1 ,
                'price' =>1200 ,
                'img' =>'/assets/product-image/1.jpeg' ,
            ],
            2=>[
                'name' => 'Haldiram Mix',
                'category_id' => 2,
                'brand_id' =>2 ,
                'price' =>45 ,
                'img' =>'/assets/product-image/2.jpg' ,
            ],

            3=>[
                'name' => 'Onion oil',
                'category_id' => 7,
                'brand_id' =>8 ,
                'price' =>700 ,
                'img' =>'/assets/product-image/8.jpg' ,
            ],
            4=>[
                'name' => 'Lipstick',
                'category_id' => 7,
                'brand_id' =>10 ,
                'price' =>700 ,
                'img' =>'/assets/product-image/9.jpg' ,
            ],
        ];
        foreach ($data as $key=>$product) {
            \App\Modules\Admin\Models\Product::insert([
                'name' => $product['name'],
                'category_id' => $product['category_id'],
                'brand_id' => $product['brand_id'],
                'price' => $product['price'],
                'img' => $product['img'],
            ]);
        }
    }
}
