<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
class Category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Electronic',
            'Food',
            'Stationary',
            'Clothing',
            'Home & Kitchen',
            'Furniture',
            'Self Care',
        ];
        foreach ($data as $category) {
            \App\Modules\Admin\Models\Category::insert([
                'name' => $category,
            ]);
        }
    }
}
