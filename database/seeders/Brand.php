<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Brand extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Boat',
            'HaldiRam',
            'Classmate',
            'Tends',
            'Softy',
            'Hewen',
            'Mother',
            'Woodland',
            'MamaaEarth',
            'Sugar Cosmetic',
        ];
        foreach ($data as $brand) {
            \App\Modules\Admin\Models\Brand::insert([
                'name' => $brand,
            ]);
        }
    }

}
