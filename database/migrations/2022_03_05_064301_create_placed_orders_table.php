<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePlacedOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('placed_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('address');
            $table->string('city');
            $table->string('zipcode');
            $table->string('name');
            $table->string('phone');
            $table->integer('quantity');
            $table->string('status')->default('pending');
            $table->unsignedBigInteger('product_id')->comment('foreign key to product table');
            $table->foreign('product_id')->references('id')->on('products')->onUpdate("CASCADE")->onDelete("NO ACTION");

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('placed_orders');
    }
}
